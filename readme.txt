=== Flamingo CSV Semicolon ===
Tags: bird, contact, mail, crm, flamingo
Requires at least: 5.9
Tested up to: 6.0
Stable tag: 1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Polish CSV export for Flamingo (encoded in Windows-1250 and delimited with semicolons).