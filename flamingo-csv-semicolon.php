<?php
/**
 * Polish CSV export for Flamingo (encoded in Windows-1250 and delimited with semicolons).
 *
 * @package Flamingo_CSV_Semicolon
 *
 * @wordpress-plugin
 * Plugin Name: Flamingo CSV Semicolon
 * Description: Polish CSV export for Flamingo (encoded in Windows-1250 and delimited with semicolons).
 * Author:      Michel Escalante Álvarez
 * Text Domain: flamingo-csv-semicolon
 * Domain Path: /languages
 * Version:     1.0.0
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Changes default encoding
 *
 * @param string $input Row input.
 * @return string
 */
function fcs_fix_flamingo_csv_quotation( $input ) {
	return iconv( 'UTF-8', 'WINDOWS-1250', $input );
}
add_filter( 'flamingo_csv_quotation', 'fcs_fix_flamingo_csv_quotation' );

/**
 * Changes comma for semicolon as values separator
 *
 * @param string $separator Separator.
 * @return string
 */
function fcs_fix_flamingo_csv_value_separator( $separator ) {
	return ';';
}
add_filter( 'flamingo_csv_value_separator', 'fcs_fix_flamingo_csv_value_separator' );
